#include <iostream>
#include <stdio.h>
#include <set>
#include <vector>
#include <map>
#include <set>
#include <algorithm>
#include <string>
#include "TriMesh.h"

using namespace std;


int it = 0;
int savingit;
int saveFrequency;
float fixedFactor;
float lastEnergy = 0;
float stopError;
float dampingFactor;
float timeStepFactor;
float constrainedPlaneParameters[7];
float cse, displTol;
int stopCounter = 3;
float dampingMultCount = 0.1;
int decreaseCount = 2;
int decreaseFrequency = 10;
float skin, growthAdd, middleGrowthAdd;
float tangentialGrowth = 1;
float middleGrowth = 1;

char saveFile[1000], saveFolder[1000], simulationStepsFolder[1000];
char meshFile[1000], growthSurface[1000], fixedSurface[1000], binaryFile[1000];
bool stop, BFS;

//global arrays needed for the simulation

//GPU arrays
float *d_growth_directions, *d_nodes, *d_forces, *d_velocity, *d_A0s, *d_normals, *d_A0s_bound, *d_bulk, *d_shear, *d_bulk_bound, *d_shear_bound, *d_growth, *d_volume, *volume, *energy;
unsigned int *d_tetras, *d_boundTetras, *d_surfaceTriangles, *d_surfaceNodes, *d_surfaceTriangles_bound, *d_closestPoint;
bool *d_directions, *d_directions_bound, *d_toTest;
char *d_nodeProp;


//CPU arrays
char* nodeProp;
float *nodes, *velocity, *finalNormals, *growth, *result, *normals, *nodes_temp, *velocity_temp, *force;
unsigned int *tetras, *surfaceTriangles, *surfaceNodes, *closestPoint;
int nodesSize, tetraSize, surfaceSize, boundTetraSize, surfaceNodesSize, surfaceSizeBound, growthSurfaceSize;

//grid dimensions
dim3 DimGridTetra;
dim3 DimGridNode;
dim3 DimGridSurface;
dim3 DimBlock;
dim3 DimGridBound;
dim3 DimBlockSurface;
dim3 DimGridBoundSurface;

//0 top layer, 1 bottom layer;
float bulks[2];
float shears[2];
float parameter[10];
float averageSpacing;

__constant__ float bulk[2]; 
__constant__ float shear[2]; 
__constant__ float parameters[10]; 
__constant__ float constrainedPlane[7];
__device__ int stopFlag;
__device__ float sumForces;
__device__ float maxPosChange;

typedef struct
{
    float3 row[3];
}matrix;

typedef struct
{
    float3 cord[4];
}nodeCoords;



/* VECTOR OPERATIONS */

__device__ float3 sum(float3 &a, float3 &b)
{
	return make_float3(a.x + b.x, a.y + b.y, a.z + b.z);
}

__device__ float3 sub(float3 &a, float3 &b)
{
	return make_float3(a.x - b.x, a.y - b.y, a.z - b.z);
}

__device__ float dot(float3 &a, float3 &b)
{
	return a.x * b.x + a.y * b.y + a.z * b.z;
}

__device__ float3 cross(float3 &a, float3 &b)
{
	float3 c;
	c.x = a.y*b.z - a.z*b.y;
	c.y = a.z*b.x - a.x*b.z;
	c.z = a.x*b.y - b.x * a.y;
	return c;
}

__device__ float3 scalar(float3 &a, float s)
{
	return make_float3(a.x * s, a.y * s, a.z * s);
}

__device__ float norm(float3 &a)
{
	return sqrt(a.x * a.x + a.y * a.y + a.z * a.z);
}

/* MATRIX OPERATIONS */

__device__ matrix copyMatrix(matrix &toCopy)
{
	matrix copy;
	copy.row[0] = make_float3(toCopy.row[0].x, toCopy.row[0].y, toCopy.row[0].z);
	copy.row[1] = make_float3(toCopy.row[1].x, toCopy.row[1].y, toCopy.row[1].z);
	copy.row[2] = make_float3(toCopy.row[2].x, toCopy.row[2].y, toCopy.row[2].z);
	return copy;
}

__device__ matrix transpose(matrix &a)
{
	matrix result;
	result.row[0] = make_float3(a.row[0].x, a.row[1].x, a.row[2].x);
	result.row[1] = make_float3(a.row[0].y, a.row[1].y, a.row[2].y);
	result.row[2] = make_float3(a.row[0].z, a.row[1].z, a.row[2].z);
	return result;
}

__device__ float det(matrix &a)
{
	float d = a.row[0].x * (a.row[1].y * a.row[2].z - a.row[1].z * a.row[2].y);
	d -= a.row[0].y * (a.row[1].x * a.row[2].z - a.row[1].z * a.row[2].x);
	d += a.row[0].z * (a.row[1].x * a.row[2].y - a.row[1].y * a.row[2].x);
	return d;
}

__device__ matrix identity()
{
	matrix I;
	I.row[0] = make_float3(1,0,0);
	I.row[1] = make_float3(0,1,0);
	I.row[2] = make_float3(0,0,1);
	return I;
}

__device__ matrix zero()
{
	matrix I;
	I.row[0] = make_float3(0,0,0);
	I.row[1] = make_float3(0,0,0);
	I.row[2] = make_float3(0,0,0);
	return I;
}

__device__ matrix inverse(matrix &a)
{
	float d = det(a);
	if(d == 0)
	{
		return zero();
	}
	else
	{
		matrix inv;
		inv.row[0] = make_float3((a.row[1].y*a.row[2].z - a.row[1].z*a.row[2].y)/d, (a.row[0].z*a.row[2].y - a.row[0].y*a.row[2].z)/d, (a.row[0].y*a.row[1].z - a.row[0].z*a.row[1].y)/d);
		inv.row[1] = make_float3((a.row[1].z*a.row[2].x - a.row[1].x*a.row[2].z)/d, (a.row[0].x*a.row[2].z - a.row[0].z*a.row[2].x)/d, (a.row[0].z*a.row[1].x - a.row[0].x*a.row[1].z)/d);
		inv.row[2] = make_float3((a.row[1].x*a.row[2].y - a.row[1].y*a.row[2].x)/d, (a.row[0].y*a.row[2].x - a.row[0].x*a.row[2].y)/d, (a.row[0].x*a.row[1].y - a.row[0].y*a.row[1].x)/d);
		return inv;
	}

}

__device__ matrix multiply(matrix &a, matrix &b)
{
	matrix c;
	matrix transB = transpose(b);
	for(int i = 0; i < 3; ++i)
	{
		c.row[i] = make_float3(dot(a.row[i], transB.row[0]), dot(a.row[i], transB.row[1]), dot(a.row[i], transB.row[2]));
	}
	return c;
}

__device__ void scalarMult(matrix *a, float scalar)
{
	for(int i = 0; i < 3; ++i)
	{
		(*a).row[i].x *= scalar;
		(*a).row[i].y *= scalar;
		(*a).row[i].z *= scalar;
	}
}

__device__ float trace(matrix &a)
{
	return a.row[0].x + a.row[1].y + a.row[2].z;
}

__device__ float3 matrixVectorMultiply(matrix &a, float3 &v, float factor)
{
	return make_float3(factor * dot(a.row[0], v), factor *  dot(a.row[1], v), factor *  dot(a.row[2], v));
}

/*Simulation*/


__device__ bool sideOfThePlane(float3 normal, float3 pointOnPlane, float3 p)
{
	if(normal.x * (p.x - pointOnPlane.x) + normal.y * (p.y - pointOnPlane.y) + normal.z * (p.z - pointOnPlane.z) > 0)
	{
		//on the side where the normal is pointing
		return true;
	}
	else return false;
}

//we take the negative value because we have force = -1 * sigma * normal pointing outwards
__device__ float4 findOutwardNormalDirections1(matrix &A0, nodeCoords &n)
{
	float4 normalDirections = make_float4(0,0,0,0);
	matrix T = transpose(A0);
	//x1 and x2
	float3 c = cross(T.row[0], T.row[1]);
	if(sideOfThePlane(c, n.cord[0], n.cord[3]))
	{
		//they are pointing in the same direction
		normalDirections.x = 1;
	}
	c = cross(T.row[1], T.row[2]);
	if(sideOfThePlane(c, n.cord[0], n.cord[1]))
	{
		//they are pointing in the same direction
		normalDirections.y = 1;
	}
	c = cross(T.row[0], T.row[2]);
	if(sideOfThePlane(c, n.cord[0], n.cord[2]))
	{
		//they are pointing in the same direction
		normalDirections.z = 1;
	}
	T.row[0] = make_float3(n.cord[2].x - n.cord[1].x, n.cord[2].y - n.cord[1].y, n.cord[2].z - n.cord[1].z);
	T.row[1] = make_float3(n.cord[3].x - n.cord[1].x, n.cord[3].y - n.cord[1].y, n.cord[3].z - n.cord[1].z);
	c = cross(T.row[0],T.row[1]);
	if(sideOfThePlane(c, n.cord[1], n.cord[0]))
	{
		//they are pointing in the same direction
		normalDirections.w = 1;
	}
	return normalDirections;
}

//we take the negative value because we have force = -1 * sigma * normal pointing outwards
__device__ float4 findOutwardNormalDirections(matrix &A0, nodeCoords &n)
{
	float4 normalDirections = make_float4(0,0,0,0);
	matrix T = transpose(A0);
	//x1 and x2
	float3 c = cross(T.row[0], T.row[1]);
	float3 test = make_float3(n.cord[3].x - n.cord[1].x, n.cord[3].y - n.cord[1].y, n.cord[3].z - n.cord[1].z);
	if(dot(c,test) > 0)
	{
		//they are pointing in the same direction
		normalDirections.x = 1;
	}
	c = cross(T.row[1], T.row[2]);
	test = make_float3(n.cord[1].x - n.cord[2].x, n.cord[1].y - n.cord[2].y, n.cord[1].z - n.cord[2].z);
	if(dot(c,test) > 0)
	{
		//they are pointing in the same direction
		normalDirections.y = 1;
	}
	c = cross(T.row[0], T.row[2]);
	test = make_float3(n.cord[2].x - n.cord[1].x, n.cord[2].y - n.cord[1].y, n.cord[2].z - n.cord[1].z);
	if(dot(c,test) > 0)
	{
		//they are pointing in the same direction
		normalDirections.z = 1;
	}
	T.row[0] = make_float3(n.cord[2].x - n.cord[1].x, n.cord[2].y - n.cord[1].y, n.cord[2].z - n.cord[1].z);
	T.row[1] = make_float3(n.cord[3].x - n.cord[1].x, n.cord[3].y - n.cord[1].y, n.cord[3].z - n.cord[1].z);
	c = cross(T.row[0],T.row[1]);
	test = make_float3(n.cord[0].x - n.cord[1].x, n.cord[0].y - n.cord[1].y, n.cord[0].z - n.cord[1].z);
	if(dot(c,test) > 0)
	{
		//they are pointing in the same direction
		normalDirections.w = 1;
	}
	return normalDirections;
}

__device__ nodeCoords getNormals(matrix &A, nodeCoords &n, float4 &normalDirections)
{
	nodeCoords normals;
	matrix T = transpose(A);
	//x1 and x2
	float3 c = cross(T.row[0], T.row[1]);
	normals.cord[0].x = normalDirections.x * c.x;
	normals.cord[0].y = normalDirections.x * c.y;
	normals.cord[0].z = normalDirections.x * c.z;
	//x2 and x3
	c = cross(T.row[1], T.row[2]);
	normals.cord[1].x = normalDirections.y * c.x;
	normals.cord[1].y = normalDirections.y * c.y;
	normals.cord[1].z = normalDirections.y * c.z;
	//x1 and x3
	c = cross(T.row[0], T.row[2]);
	normals.cord[2].x = normalDirections.z * c.x;
	normals.cord[2].y = normalDirections.z * c.y;
	normals.cord[2].z = normalDirections.z * c.z;
	//the rest
	T.row[0] = make_float3(n.cord[2].x - n.cord[1].x, n.cord[2].y - n.cord[1].y, n.cord[2].z - n.cord[1].z);
	T.row[1] = make_float3(n.cord[3].x - n.cord[1].x, n.cord[3].y - n.cord[1].y, n.cord[3].z - n.cord[1].z);	
	c = cross(T.row[0],T.row[1]);
	normals.cord[3].x = normalDirections.w * c.x;
	normals.cord[3].y = normalDirections.w * c.y;
	normals.cord[3].z = normalDirections.w * c.z;
	return normals;	
}


__device__ float3 getTetraCentroid(nodeCoords &n)
{
	float3 r = make_float3((n.cord[0].x + n.cord[1].x + n.cord[2].x + n.cord[3].x)/4, (n.cord[0].y + n.cord[1].y + n.cord[2].y + n.cord[3].y)/4, (n.cord[0].z + n.cord[1].z + n.cord[2].z + n.cord[3].z)/4);
	return r;
}

__device__ float3 getTriangleCentroid(float3 a, float3 b, float3 c)
{
	float3 r = make_float3((a.x + b.x + c.x)/3, (a.y + b.y + c.y)/3, (a.z + b.z + c.z)/3);
	return r;
}


__device__ float growthFunctionDistance(float distance)
{
	
	float b = (parameters[5] * (1 - parameters[6]))/(parameters[5]*parameters[6] - 1);
	float x = parameters[4] - distance;
	b = 1 + (parameters[5] - 1)/(1 + b * expf(-1 * parameters[7] * x));
	if(b < 1) return 1;
	if(b > parameters[5]) return parameters[5];
	return b;	
}

__device__ float distanceToPlane(float3 a)
{
	return abs(constrainedPlane[0] * a.x + constrainedPlane[1] * a.y + constrainedPlane[2] * a.z + constrainedPlane[3])/sqrt(constrainedPlane[0]*constrainedPlane[0] + constrainedPlane[1]*constrainedPlane[1] + constrainedPlane[2]*constrainedPlane[2]);
}

__device__ float growthFunctionDistanceMiddle(float distance)
{
	float b = (parameters[9] * (1 - parameters[6]))/(parameters[9]*parameters[6] - 1);
	float x =  parameters[4] - distance;
	b = 1 + (parameters[9] - 1)/(1 + b * expf(-1 * parameters[7] * x));
	if(b < 1) return 1;
	if(b > parameters[9]) return parameters[9];
	return b;
}

__device__ float growthFunctionDistanceMiddleAdd(float distance, float value)
{
	float b = (value * (1 - parameters[6]))/(value*parameters[6] - 1);
	float x =  parameters[4] - distance;
	b = 1 + (value - 1)/(1 + b * expf(-1 * parameters[7] * x));
	if(b < 1) return 1;
	if(b > value) return value;
	return b;
}


//first element is shear modulus, second is bulk modulus
//middle nodes are all considered havig the bottom layer properties
__device__ float2 getTetraProperties(char n1, char n2, char n3, char n4)
{
	float onTheTopLayer = 0;
	if(n1 == 's' || n1 == 't') onTheTopLayer += 1.0;
	if(n2 == 's' || n2 == 't') onTheTopLayer += 1.0;
	if(n3 == 's' || n3 == 't') onTheTopLayer += 1.0;
	if(n4 == 's' || n4 == 't') onTheTopLayer += 1.0;
	int consider = 4;
	if(n1 == 'm') consider--;
	if(n2 == 'm') consider--;
	if(n3 == 'm') consider--;
	if(n4 == 'm') consider--;
	if(consider == 0) consider = 1;
	float2 result = make_float2(0,0);
	result.x = (onTheTopLayer/consider) * bulk[0] + (1 - onTheTopLayer/consider) * bulk[1];
	result.y = (onTheTopLayer/consider) * shear[0] + (1 - onTheTopLayer/consider) * shear[1];
	return result;
}

__device__ float changeStiffness(char n1, char n2, char n3, char n4, float factor)
{
	float onTheTopLayer = 0;
	if(n1 == 's' || n1 == 't') onTheTopLayer += 1.0;
	if(n2 == 's' || n2 == 't') onTheTopLayer += 1.0;
	if(n3 == 's' || n3 == 't') onTheTopLayer += 1.0;
	if(n4 == 's' || n4 == 't') onTheTopLayer += 1.0;
	if(onTheTopLayer == 0) return factor;
	else return 1;
}

__device__ bool isInTheMiddle(char n1, char n2, char n3, char n4)
{

	if(n1 == 'm') return true;
	if(n2 == 'm') return true;
	if(n3 == 'm') return true;
	if(n4 == 'm') return true;
	return false;

}

void checkMemoryConsumption()
{
	    // show memory usage of GPU

        size_t free_byte ;

        size_t total_byte ;

        cudaError_t cuda_status = cudaMemGetInfo( &free_byte, &total_byte ) ;

        if ( cudaSuccess != cuda_status ){

            printf("Error: cudaMemGetInfo fails, %s \n", cudaGetErrorString(cuda_status) );

            exit(1);

        }



        double free_db = (double)free_byte ;

        double total_db = (double)total_byte ;

        double used_db = total_db - free_db ;

        printf("GPU memory usage: used = %f, free = %f MB, total = %f MB\n",

            used_db/1024.0/1024.0, free_db/1024.0/1024.0, total_db/1024.0/1024.0);
}

float getAverageForce()
{
	float sum = 0;
	float count = 0;
	for(int i = 0; i < nodesSize; ++i)
	{
		if(nodeProp[i] != 'f')
		{
			sum += force[3 * i] * force[3 * i] + force[3 * i + 2] * force[3 * i + 2];
			if(nodeProp[i] != 'm') sum += force[3 * i + 1] * force[3 * i + 1];
			count++;
		}
	}
	return sum/count;
}

float getForceStd(float avg)
{
	float suma = 0;
	float count = 0;
	for(int i = 0; i < nodesSize; ++i)
	{
		if(nodeProp[i] != 'f')
		{
			float sum = 0;
			sum += force[3 * i] * force[3 * i] + force[3 * i + 2] * force[3 * i + 2];
			if(nodeProp[i] != 'm') sum += force[3 * i + 1] * force[3 * i + 1];
			suma += (sum - avg);
			count++;
		}
	}
	return suma/count;
}

void tetraCentroid(int i, float *cx, float *cy, float *cz)
{
	*cx = 0; *cy = 0; *cz = 0;
	for(int j = 0; j < 4; ++j)
	{
		*cx += nodes[3 * tetras[4 * i + j]];
		*cy += nodes[3 * tetras[4 * i + j] + 1];
		*cz += nodes[3 * tetras[4 * i + j] + 2];				
	}
	*cx/= 4.0;
	*cy/= 4.0;
	*cz/= 4.0;
	
}
