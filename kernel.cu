#include "data.cu"

using namespace std;
using namespace trimesh;



__global__ void prepareSimulation(float *nodes, unsigned int *tetras, float *A0s, bool *normalDirections, int tetraSize, char *nodeProperties, float *d_bulk, float *d_shear, float *d_growth, unsigned int *d_closestPoint, float *d_normals, float *d_growth_directions, float *d_volume)
{
	int id = threadIdx.x + blockDim.x * blockIdx.x;	
	if(id >= tetraSize) return;
	int4 n_index = make_int4(tetras[id * 4], tetras[id * 4 + 1], tetras[id * 4 + 2], tetras[id * 4 + 3]);
	nodeCoords n;
	n.cord[0] = make_float3(nodes[n_index.x * 3], nodes[n_index.x * 3 + 1], nodes[n_index.x * 3 + 2]);
	n.cord[1] = make_float3(nodes[n_index.y * 3], nodes[n_index.y * 3 + 1], nodes[n_index.y * 3 + 2]);
	n.cord[2] = make_float3(nodes[n_index.z * 3], nodes[n_index.z * 3 + 1], nodes[n_index.z * 3 + 2]);
	n.cord[3] = make_float3(nodes[n_index.w * 3], nodes[n_index.w * 3 + 1], nodes[n_index.w * 3 + 2]);
	matrix G;
	float g = growthFunctionDistance(d_growth[id]);
	d_growth[id] = g;
	int closestPoint = d_closestPoint[id];
	float3 growthDirection = make_float3(d_normals[3 * closestPoint], d_normals[3 * closestPoint + 1], d_normals[3 * closestPoint + 2]);
	float nor = norm(growthDirection);
	growthDirection = scalar(growthDirection, 1/nor);
	G.row[0] = make_float3((1 - g) * growthDirection.x * growthDirection.x + g, (1 - g) * growthDirection.x * growthDirection.y,  (1 - g) * growthDirection.x * growthDirection.z);
	G.row[1] = make_float3((1 - g) * growthDirection.x * growthDirection.y, (1 - g) * growthDirection.y * growthDirection.y + g, (1 - g) * growthDirection.y * growthDirection.z);
	G.row[2] = make_float3((1 - g) * growthDirection.x * growthDirection.z, (1 - g) * growthDirection.y * growthDirection.z, (1 - g) * growthDirection.z * growthDirection.z + g);		
	d_growth_directions[3 * id] = growthDirection.x;
	d_growth_directions[3 * id + 1] = growthDirection.y;
	d_growth_directions[3 * id + 2] = growthDirection.z;	
	float3 centroid = getTetraCentroid(n);
	float middleG = growthFunctionDistanceMiddle(distanceToPlane(centroid));

	if(middleG > 1)
	{
		matrix temp;
		growthDirection.x = constrainedPlane[4];
		growthDirection.y = constrainedPlane[5];
		growthDirection.z = constrainedPlane[6];
		temp.row[0] = make_float3((1 - middleG) * growthDirection.x * growthDirection.x + middleG, (1 - g) * growthDirection.x * growthDirection.y,  (1 - middleG) * growthDirection.x * growthDirection.z);
		temp.row[1] = make_float3((1 - middleG) * growthDirection.x * growthDirection.y, (1 - middleG) * growthDirection.y * growthDirection.y + middleG, (1 - middleG) * growthDirection.y * growthDirection.z);
		temp.row[2] = make_float3((1 - middleG) * growthDirection.x * growthDirection.z, (1 - middleG) * growthDirection.y * growthDirection.z, (1 - middleG) * growthDirection.z * growthDirection.z + middleG);		
		G = multiply(temp,G);		
	}	
	matrix A0;
	A0.row[0] = make_float3(n.cord[1].x - n.cord[0].x, n.cord[2].x - n.cord[0].x, n.cord[3].x - n.cord[0].x);
 	A0.row[1] = make_float3(n.cord[1].y - n.cord[0].y, n.cord[2].y - n.cord[0].y, n.cord[3].y - n.cord[0].y);
 	A0.row[2] = make_float3(n.cord[1].z - n.cord[0].z, n.cord[2].z - n.cord[0].z, n.cord[3].z - n.cord[0].z);
 	d_volume[id] = det(A0)/6;
 	float4 directions = findOutwardNormalDirections(A0, n);
 	A0 = multiply(G, A0);
 	A0 = inverse(A0);
 	//save A0G-1
 	A0s[id * 9] = A0.row[0].x;
 	A0s[id * 9 + 1] = A0.row[0].y;
 	A0s[id * 9 + 2] = A0.row[0].z;
 	A0s[id * 9 + 3] = A0.row[1].x;
 	A0s[id * 9 + 4] = A0.row[1].y;
 	A0s[id * 9 + 5] = A0.row[1].z;
 	A0s[id * 9 + 6] = A0.row[2].x;
 	A0s[id * 9 + 7] = A0.row[2].y;
 	A0s[id * 9 + 8] = A0.row[2].z;
 	//save normal directions
 	normalDirections[id * 4] = (bool)directions.x;
 	normalDirections[id * 4 + 1] = (bool)directions.y;
	normalDirections[id * 4 + 2] = (bool)directions.z;
	normalDirections[id * 4 + 3] = (bool)directions.w;
	
	float2 properties = getTetraProperties(nodeProperties[n_index.x], nodeProperties[n_index.y], nodeProperties[n_index.z], nodeProperties[n_index.w]);
	d_bulk[id] = properties.x;
	d_shear[id] = properties.y; 
}

__global__ void addMoreGrowth(float *nodes, unsigned int *tetras,int tetraSize, float addGrowth, float addGrowthMiddle, float *d_growth, float *d_growth_directions, float *A0s, char *nodeProperties)
{
	int id = threadIdx.x + blockDim.x * blockIdx.x;	
	if(id >= tetraSize) return;
	matrix G;
	float g = 1;
	int4 n_index = make_int4(tetras[id * 4], tetras[id * 4 + 1], tetras[id * 4 + 2], tetras[id * 4 + 3]);
	float3 growthDirection = make_float3(d_growth_directions[3 * id], d_growth_directions[3 * id + 1], d_growth_directions[3 * id + 2]);
	if(d_growth[id] > 1)
	{
		g = addGrowth;
		d_growth[id] = d_growth[id] * g;
	}
	G.row[0] = make_float3((1 - g) * growthDirection.x * growthDirection.x + g, (1 - g) * growthDirection.x * growthDirection.y,  (1 - g) * growthDirection.x * growthDirection.z);
	G.row[1] = make_float3((1 - g) * growthDirection.x * growthDirection.y, (1 - g) * growthDirection.y * growthDirection.y + g, (1 - g) * growthDirection.y * growthDirection.z);
	G.row[2] = make_float3((1 - g) * growthDirection.x * growthDirection.z, (1 - g) * growthDirection.y * growthDirection.z, (1 - g) * growthDirection.z * growthDirection.z + g);		
	nodeCoords n;
	n.cord[0] = make_float3(nodes[n_index.x * 3], nodes[n_index.x * 3 + 1], nodes[n_index.x * 3 + 2]);
	n.cord[1] = make_float3(nodes[n_index.y * 3], nodes[n_index.y * 3 + 1], nodes[n_index.y * 3 + 2]);
	n.cord[2] = make_float3(nodes[n_index.z * 3], nodes[n_index.z * 3 + 1], nodes[n_index.z * 3 + 2]);
	n.cord[3] = make_float3(nodes[n_index.w * 3], nodes[n_index.w * 3 + 1], nodes[n_index.w * 3 + 2]);
	
	float3 centroid = getTetraCentroid(n);	
	float middleG = growthFunctionDistanceMiddleAdd(distanceToPlane(centroid), addGrowthMiddle);

	if(middleG > 1)
	{
		
		matrix temp;
		growthDirection.x = constrainedPlane[4];
		growthDirection.y = constrainedPlane[5];
		growthDirection.z = constrainedPlane[6];
		temp.row[0] = make_float3((1 - middleG) * growthDirection.x * growthDirection.x + middleG, (1 - g) * growthDirection.x * growthDirection.y,  (1 - middleG) * growthDirection.x * growthDirection.z);
		temp.row[1] = make_float3((1 - middleG) * growthDirection.x * growthDirection.y, (1 - middleG) * growthDirection.y * growthDirection.y + middleG, (1 - middleG) * growthDirection.y * growthDirection.z);
		temp.row[2] = make_float3((1 - middleG) * growthDirection.x * growthDirection.z, (1 - middleG) * growthDirection.y * growthDirection.z, (1 - middleG) * growthDirection.z * growthDirection.z + middleG);		
		G = multiply(temp,G);	
	}
	G = inverse(G);
	matrix A0;
	
 	A0.row[0].x = A0s[id * 9];
 	A0.row[0].y = A0s[id * 9 + 1];
 	A0.row[0].z = A0s[id * 9 + 2];

 	A0.row[1].x = A0s[id * 9 + 3];
 	A0.row[1].y = A0s[id * 9 + 4];
 	A0.row[1].z = A0s[id * 9 + 5];
 
  	A0.row[2].x = A0s[id * 9 + 6];
 	A0.row[2].y = A0s[id * 9 + 7];
 	A0.row[2].z = A0s[id * 9 + 8];
 	
 	A0 = multiply(A0,G);

 	A0s[id * 9] = A0.row[0].x;
 	A0s[id * 9 + 1] = A0.row[0].y;
 	A0s[id * 9 + 2] = A0.row[0].z;
 	A0s[id * 9 + 3] = A0.row[1].x;
 	A0s[id * 9 + 4] = A0.row[1].y;
 	A0s[id * 9 + 5] = A0.row[1].z;
 	A0s[id * 9 + 6] = A0.row[2].x;
 	A0s[id * 9 + 7] = A0.row[2].y;
 	A0s[id * 9 + 8] = A0.row[2].z;
 	
}


__global__ void getNodeForces(float *nodes, unsigned int *tetras, float *A0s, bool *normalDirections, int tetraSize, float *force, float *d_bulk, float *d_shear, float *d_volume)
{
	int id = threadIdx.x + blockDim.x * blockIdx.x;
	if(id >= tetraSize) return;
	matrix A0; 

	A0.row[0] = make_float3(A0s[id * 9], A0s[id * 9 + 1], A0s[id * 9 + 2]);
	A0.row[1] = make_float3(A0s[id * 9 + 3], A0s[id * 9 + 4], A0s[id * 9 + 5]);
	A0.row[2] = make_float3(A0s[id * 9 + 6], A0s[id * 9 + 7], A0s[id * 9 + 8]);
	int4 n_index = make_int4(tetras[id * 4], tetras[id * 4 + 1], tetras[id * 4 + 2], tetras[id * 4 + 3]);
	nodeCoords n;
	
	n.cord[0] = make_float3(nodes[n_index.x * 3], nodes[n_index.x * 3 + 1], nodes[n_index.x * 3 + 2]);
	n.cord[1] = make_float3(nodes[n_index.y * 3], nodes[n_index.y * 3 + 1], nodes[n_index.y * 3 + 2]);
	n.cord[2] = make_float3(nodes[n_index.z * 3], nodes[n_index.z * 3 + 1], nodes[n_index.z * 3 + 2]);
	n.cord[3] = make_float3(nodes[n_index.w * 3], nodes[n_index.w * 3 + 1], nodes[n_index.w * 3 + 2]);	
	matrix A;
	A.row[0] = make_float3(n.cord[1].x - n.cord[0].x, n.cord[2].x - n.cord[0].x, n.cord[3].x - n.cord[0].x);
	A.row[1] = make_float3(n.cord[1].y - n.cord[0].y, n.cord[2].y - n.cord[0].y, n.cord[3].y - n.cord[0].y);
	A.row[2] = make_float3(n.cord[1].z - n.cord[0].z, n.cord[2].z - n.cord[0].z, n.cord[3].z - n.cord[0].z);
	float energy = 0;	
	float shear = d_shear[id];
	matrix sigma = multiply(A,A0); //F
	float J = det(sigma);	


	matrix sigmaT = transpose(sigma);
	//sigma is now F*F^T
	sigma = multiply(sigma, sigmaT); //F*FT
	energy = trace(sigma);
	float t = -1 * energy/3;
	sigma.row[0].x += t;
	sigma.row[1].y += t;
	sigma.row[2].z += t;
	t = shear/powf(J, 5.0/3);
	energy = shear/2 * (energy/powf(J, 2.0/3) - 3);
	scalarMult(&sigma, t);
	//calculate bulk modulus
	t = d_bulk[id]*(J - 1);
	energy += t * (J - 1);
	//save the energy of the tetra
	d_volume[id] = energy;
	sigma.row[0].x += 2 * t;
	sigma.row[1].y += 2 * t;
	sigma.row[2].z += 2 * t;
	float4 directions = make_float4(-1,-1,-1,-1);
	if(normalDirections[id * 4]) directions.x = 1;
	if(normalDirections[id * 4 + 1]) directions.y = 1;
	if(normalDirections[id * 4 + 2]) directions.z = 1;
	if(normalDirections[id * 4 + 3]) directions.w = 1;
	nodeCoords normals = getNormals(A, n, directions);

	for(int i = 0; i < 4; ++i)
	{
		normals.cord[i] = matrixVectorMultiply(sigma, normals.cord[i], 1.0/3);
	}
			//set forces of all nodes
	nodeCoords forces;
	forces.cord[0].x = normals.cord[0].x + normals.cord[1].x + normals.cord[2].x;
	forces.cord[0].y = normals.cord[0].y + normals.cord[1].y + normals.cord[2].y;
	forces.cord[0].z = normals.cord[0].z + normals.cord[1].z + normals.cord[2].z;
	atomicAdd(&force[n_index.x * 3], forces.cord[0].x);
	atomicAdd(&force[n_index.x * 3 + 1], forces.cord[0].y);
	atomicAdd(&force[n_index.x * 3 + 2], forces.cord[0].z);

	forces.cord[1].x = normals.cord[0].x + normals.cord[2].x + normals.cord[3].x;
	forces.cord[1].y = normals.cord[0].y + normals.cord[2].y + normals.cord[3].y;
	forces.cord[1].z = normals.cord[0].z + normals.cord[2].z + normals.cord[3].z;
	atomicAdd(&force[n_index.y * 3], forces.cord[1].x);
	atomicAdd(&force[n_index.y * 3 + 1], forces.cord[1].y);
	atomicAdd(&force[n_index.y * 3 + 2], forces.cord[1].z);	

	forces.cord[2].x = normals.cord[0].x + normals.cord[1].x + normals.cord[3].x;
	forces.cord[2].y = normals.cord[0].y + normals.cord[1].y + normals.cord[3].y;
	forces.cord[2].z = normals.cord[0].z + normals.cord[1].z + normals.cord[3].z;
	atomicAdd(&force[n_index.z * 3], forces.cord[2].x);
	atomicAdd(&force[n_index.z * 3 + 1], forces.cord[2].y);
	atomicAdd(&force[n_index.z * 3 + 2], forces.cord[2].z);	

	forces.cord[3].x = normals.cord[1].x + normals.cord[2].x + normals.cord[3].x;
	forces.cord[3].y = normals.cord[1].y + normals.cord[2].y + normals.cord[3].y;
	forces.cord[3].z = normals.cord[1].z + normals.cord[2].z + normals.cord[3].z;
	atomicAdd(&force[n_index.w * 3], forces.cord[3].x);
	atomicAdd(&force[n_index.w * 3 + 1], forces.cord[3].y);
	atomicAdd(&force[n_index.w * 3 + 2], forces.cord[3].z);
}


    


__global__ void updatePositions(float *nodes, int nodesSize, float *velocity, float *force, char *nodeProperties)
{
	int id = threadIdx.x + blockDim.x * blockIdx.x;
	if(id >= nodesSize || nodeProperties[id] == 'f' ) return;
	float3 myNode = make_float3(nodes[id * 3], nodes[id * 3 + 1], nodes[id*3 + 2]);	
	float3 oldValue = make_float3(myNode.x, myNode.y, myNode.z);
	float3 f = make_float3(force[id*3], force[id*3 + 1], force[id*3 + 2]);
	//nodes close to the middle can't move in the y direction
	if(nodeProperties[id] == 'm') 
	{
		float3 to_add = make_float3(f.x, -1 *f.y, f.z);
		f = sum(f, to_add);
	}
	float3 v = make_float3(velocity[id*3], velocity[id*3 + 1], velocity[id*3 + 2]);
	float3 dump = scalar(v, -1 * parameters[1]);
	f = sum(f, dump);
	//atomicAdd(&sumForces, f.x * f.x + f.y*f.y + f.z*f.z);
	f = scalar(f,parameters[2]/parameters[3]);
	v = sum(v, f);
	f = scalar(v, parameters[2]);
	myNode = sum(myNode, f);

	nodes[id * 3] = myNode.x;
	nodes[id * 3 + 1] = myNode.y;
	nodes[id * 3 + 2] = myNode.z;
	velocity[id * 3] = v.x;
	velocity[id * 3 + 1] = v.y;
	velocity[id * 3 + 2] = v.z;
	//here we take care of the middle nodes, and we do not update the y coordinate
}

__global__ void checkStopping(int nodesSize, float *force, char *nodeProperties)
{
	int id = threadIdx.x + blockDim.x * blockIdx.x;
	if(id >= nodesSize || nodeProperties[id] == 'f') return;
	float3 f = make_float3(force[id*3], force[id*3 + 1], force[id*3 + 2]);
	atomicAdd(&sumForces, f.x * f.x + f.y*f.y + f.z*f.z);
}



void freeCuda()
{
	cudaFree(d_nodes);
	cudaFree(d_tetras);
	cudaFree(d_forces);
	cudaFree(d_velocity);
	cudaFree(d_A0s);
	cudaFree(d_directions);
	cudaFree(d_normals);
}


bool initializeCuda()
{
	
	nodes_temp = (float*)malloc(3 * nodesSize * sizeof(float));
	force = (float*)malloc(nodesSize*3*sizeof(float));
	velocity_temp = (float*)malloc(nodesSize*3*sizeof(float));
	velocity = (float*)malloc(nodesSize*3*sizeof(float));	
	memset(nodes_temp, 0, nodesSize*3*sizeof(float));
	memset(velocity_temp, 0, nodesSize*3*sizeof(float));
	memset(velocity, 0, nodesSize*3*sizeof(float));
	memset(force, 0, nodesSize*3*sizeof(float));
	cudaMalloc((void**) &d_nodes, nodesSize*3*sizeof(float));
	cudaError_t err = cudaMalloc((void**) &d_forces, nodesSize*3*sizeof(float));
	if (err != cudaSuccess) printf("%s\n", cudaGetErrorString(err));
	err =  cudaMemcpy(d_forces, force, nodesSize*3*sizeof(float), cudaMemcpyHostToDevice);
	if (err != cudaSuccess) printf("%s\n", cudaGetErrorString(err));
	
	cudaMalloc((void**) &d_velocity, nodesSize*3*sizeof(float));
	cudaMemcpy(d_velocity, force, nodesSize*3*sizeof(float), cudaMemcpyHostToDevice);

	cudaMalloc((void**) &d_A0s, tetraSize*9*sizeof(float));
	cudaMalloc((void**) &d_directions, tetraSize*4*sizeof(bool));

	cudaMalloc((void**) &d_surfaceTriangles, surfaceSize*3*sizeof(unsigned int));
	cudaMemcpy(d_surfaceTriangles, surfaceTriangles, surfaceSize*3*sizeof(unsigned int), cudaMemcpyHostToDevice);

	cudaMalloc((void**) &d_bulk, tetraSize*sizeof(float));
	cudaMalloc((void**) &d_shear, tetraSize*sizeof(float));
	
	cudaMalloc((void**) &d_nodeProp, nodesSize*sizeof(char));
	cudaMemcpy(d_nodeProp, nodeProp, nodesSize*sizeof(char), cudaMemcpyHostToDevice);
	
	//cudaMalloc((void**) &d_nodes, nodesSize*3*sizeof(float));
	err = cudaMemcpy(d_nodes, nodes, nodesSize*3*sizeof(float), cudaMemcpyHostToDevice);
	if (err != cudaSuccess) printf("%s\n", cudaGetErrorString(err));

	cudaMalloc((void**) &d_tetras, tetraSize*4*sizeof(unsigned int));
	cudaMemcpy(d_tetras, tetras, tetraSize*4*sizeof(unsigned int), cudaMemcpyHostToDevice);
	cudaMalloc((void**) &d_growth, tetraSize*sizeof(float));
	cudaMemcpy(d_growth, growth, tetraSize * sizeof(float), cudaMemcpyHostToDevice);	
	cudaMalloc((void**) &d_closestPoint, tetraSize*sizeof(unsigned int));
	cudaMemcpy(d_closestPoint, closestPoint, tetraSize * sizeof(unsigned int), cudaMemcpyHostToDevice);
	cudaMalloc((void**) &d_normals, growthSurfaceSize*3*sizeof(float));
	cudaMemcpy(d_normals, normals, growthSurfaceSize * 3 * sizeof(float), cudaMemcpyHostToDevice);
	cudaMalloc((void**) &d_growth_directions, tetraSize*3*sizeof(float));
	cudaMalloc((void**) &d_volume, tetraSize*sizeof(float));
	

	cudaMemcpyToSymbol(bulk, bulks, 2 * sizeof(float));
	cudaMemcpyToSymbol(shear, shears, 2 * sizeof(float));
	cudaMemcpyToSymbol(parameters, parameter, 10 * sizeof(float));
	cudaMemcpyToSymbol(constrainedPlane, constrainedPlaneParameters, 7 * sizeof(float));
	DimGridTetra = dim3((tetraSize - 1)/1024 + 1, 1, 1);
	DimGridNode = dim3((nodesSize - 1)/1024 + 1, 1, 1);
	DimGridSurface = dim3((surfaceNodesSize - 1)/32 + 1, (surfaceSize - 1)/32 + 1, 1);
	DimBlock = dim3(1024,1,1);
	DimBlockSurface = dim3(32,32,1);
	
	cout << "kernel invocation" << endl;
	prepareSimulation<<<DimGridTetra, DimBlock>>>(d_nodes, d_tetras, d_A0s, d_directions, tetraSize, d_nodeProp, d_bulk, d_shear, d_growth, d_closestPoint, d_normals, d_growth_directions, d_volume);
	volume = (float*)malloc(tetraSize*sizeof(float));
	energy = (float*)malloc(tetraSize*sizeof(float));
	memset(volume, 0, tetraSize*sizeof(float));
	memset(energy, 0, tetraSize*sizeof(float));
	cudaMemcpy(volume, d_volume, tetraSize*sizeof(float), cudaMemcpyDeviceToHost);
	if (err != cudaSuccess) printf("%s\n", cudaGetErrorString(err));
	return true;
}

bool initializeCudaFromBinaryFile(const char *filename)
{
	fstream file(filename, ios::in | ios::binary);
	if(!file)
	{
		cout << "No bin file to open" << endl;
		return false;
	}
	file.read((char*)&nodesSize, sizeof(int));
	nodes = (float*)malloc(nodesSize * 3 * sizeof(float));
	cout << "Number of nodes: " << nodesSize << endl;
	file.read ((char*)nodes, 3 * nodesSize * sizeof(float));
	file.read ((char*)&tetraSize, sizeof(int));
	tetras = (unsigned int*)malloc(tetraSize * 4 * sizeof(unsigned int));
	file.read((char*)tetras, tetraSize * 4 * sizeof(unsigned int));
	cout << "Number of tetrahedrons: " << tetraSize << endl;
	float *A0s = (float*)malloc(tetraSize*9*sizeof(float));
	file.read((char*)A0s, tetraSize * 9 * sizeof(float));
	
	bool *directions = (bool*)malloc(tetraSize*4*sizeof(bool));
	file.read((char*)directions, tetraSize * 4 * sizeof(bool));
	
	float *bulk = (float*)malloc(tetraSize * sizeof(float));
	file.read((char*)bulk, tetraSize * sizeof(float));
	
	float *shear = (float*)malloc(tetraSize * sizeof(float));
	file.read((char*)shear, tetraSize * sizeof(float));	
	
	velocity = (float*)malloc(nodesSize * 3 * sizeof(float));
	file.read((char*)velocity, nodesSize * 3 * sizeof(float));	
	
	file.read ((char*)&averageSpacing, sizeof(float));
	cout << "Average mesh spacing: " << averageSpacing << endl;
	file.read ((char*)&surfaceSize, sizeof(int));
	surfaceTriangles = (unsigned int*)malloc(surfaceSize * 3 * sizeof(unsigned int));
	file.read((char*)surfaceTriangles, surfaceSize * 3 * sizeof(unsigned int));
	cout << "Surface Size: " << surfaceSize << endl;
	nodeProp = (char*)malloc(nodesSize * sizeof(char));	
	file.read((char*)nodeProp, nodesSize * sizeof(char));

	file.read((char*)parameter, 10 * sizeof(float));
	file.read((char*)constrainedPlaneParameters, 7 * sizeof(float));
	
	float *gd = (float*)malloc(tetraSize*3*sizeof(float));
	float *gr = (float*)malloc(tetraSize*sizeof(float));
	
	file.read((char*)gd, tetraSize*3*sizeof(float));
	file.read((char*)gr, tetraSize*sizeof(float));

	volume = (float*)malloc(tetraSize * sizeof(float));
	energy = (float*)malloc(tetraSize*sizeof(float));
	memset(volume, 0, tetraSize*sizeof(float));
	memset(energy, 0, tetraSize*sizeof(float));
	cudaMalloc((void**) &d_volume, tetraSize*sizeof(float));
	file.read((char*)volume, tetraSize*sizeof(float));
	
	
	file.close();
	
	cout << "Configuration loaded from: " << filename << endl;	
	
	//transfer the parameters to GPU
	
	nodes_temp = (float*)malloc(3 * nodesSize * sizeof(float));
	force = (float*)malloc(nodesSize*3*sizeof(float));
	velocity_temp = (float*)malloc(nodesSize*3*sizeof(float));
		
	memset(nodes_temp, 0, nodesSize*3*sizeof(float));
	memset(velocity_temp, 0, nodesSize*3*sizeof(float));
	memset(force, 0, nodesSize*3*sizeof(float));
	
	cudaMalloc((void**) &d_nodes, nodesSize*3*sizeof(float));
	cudaMalloc((void**) &d_forces, nodesSize*3*sizeof(float));
	cudaMemcpy(d_forces, force, nodesSize*3*sizeof(float), cudaMemcpyHostToDevice);
	
	cudaMalloc((void**) &d_velocity, nodesSize*3*sizeof(float));
	cudaMemcpy(d_velocity, velocity, nodesSize*3*sizeof(float), cudaMemcpyHostToDevice);

	cudaMalloc((void**) &d_A0s, tetraSize*9*sizeof(float));
	cudaMemcpy(d_A0s, A0s, tetraSize*9*sizeof(float), cudaMemcpyHostToDevice);
	
	cudaMalloc((void**) &d_directions, tetraSize*4*sizeof(bool));
	cudaMemcpy(d_directions, directions,tetraSize*4*sizeof(bool), cudaMemcpyHostToDevice);

	cudaMalloc((void**) &d_surfaceTriangles, surfaceSize*3*sizeof(unsigned int));
	cudaMemcpy(d_surfaceTriangles, surfaceTriangles, surfaceSize*3*sizeof(unsigned int), cudaMemcpyHostToDevice);

	cudaMalloc((void**) &d_bulk, tetraSize*sizeof(float));
	cudaMemcpy(d_bulk, bulk,tetraSize*sizeof(float), cudaMemcpyHostToDevice);
	
	cudaMalloc((void**) &d_shear, tetraSize*sizeof(float));
	cudaMemcpy(d_shear, shear,tetraSize*sizeof(float), cudaMemcpyHostToDevice);
		
	cudaMalloc((void**) &d_nodeProp, nodesSize*sizeof(char));
	cudaMemcpy(d_nodeProp, nodeProp, nodesSize*sizeof(char), cudaMemcpyHostToDevice);
	
	cudaMemcpy(d_nodes, nodes, nodesSize*3*sizeof(float), cudaMemcpyHostToDevice);

	cudaMalloc((void**) &d_tetras, tetraSize*4*sizeof(unsigned int));
	cudaMemcpy(d_tetras, tetras, tetraSize*4*sizeof(unsigned int), cudaMemcpyHostToDevice);
		
	cudaMalloc((void**) &d_growth_directions, tetraSize*3*sizeof(float));
	cudaMalloc((void**) &d_growth, tetraSize*sizeof(float));
	
	cudaMemcpy(d_growth_directions, gd, tetraSize * 3 * sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(d_growth, gr, tetraSize * sizeof(float), cudaMemcpyHostToDevice);
	free(gd);
	free(gr);

	cudaMemcpyToSymbol(parameters, parameter, 10 * sizeof(float));
	cudaMemcpyToSymbol(constrainedPlane, constrainedPlaneParameters, 7 * sizeof(float));
	DimGridTetra = dim3((tetraSize - 1)/1024 + 1, 1, 1);
	DimGridNode = dim3((nodesSize - 1)/1024 + 1, 1, 1);
	DimGridSurface = dim3((surfaceNodesSize - 1)/32 + 1, (surfaceSize - 1)/32 + 1, 1);
	DimBlock = dim3(1024,1,1);
	DimBlockSurface = dim3(32,32,1);
	
	return true;
}

void addGrowth(float g, float mg)
{

	addMoreGrowth<<<DimGridTetra, DimBlock>>>(d_nodes, d_tetras, tetraSize, g, mg, d_growth, d_growth_directions, d_A0s, d_nodeProp);
	cudaDeviceSynchronize();
}


void iteration(int it)
{	
	cudaMemset(d_forces, 0, nodesSize*3*sizeof(float));
	getNodeForces<<<DimGridTetra, DimBlock>>>(d_nodes, d_tetras, d_A0s, d_directions, tetraSize, d_forces, d_bulk, d_shear, d_volume);
	cudaDeviceSynchronize();
	updatePositions<<<DimGridNode, DimBlock>>>(d_nodes, nodesSize, d_velocity, d_forces, d_nodeProp);
	cudaDeviceSynchronize();
	//cout << "Iteration: " <<cudaGetErrorName(cudaGetLastError()) << endl;

}

float checkForStopping()
{
	float sum = 0;
	cudaMemcpyToSymbol(sumForces,&sum, sizeof(float), 0, cudaMemcpyHostToDevice); 
	checkStopping<<<DimGridNode, DimBlock>>>(nodesSize, d_forces, d_nodeProp);
	cudaDeviceSynchronize();
	cudaMemcpyFromSymbol(&sum, sumForces, sizeof(float), 0, cudaMemcpyDeviceToHost);
	return sum;
}


void changeDamping(float factor)
{
	parameter[1] *= factor;
	cudaMemcpyToSymbol(parameters, parameter, 10 * sizeof(float));
	
}

void saveTheSurface(char* folder, char *file)
{
	cudaMemcpy(nodes, d_nodes, nodesSize*3*sizeof(float), cudaMemcpyDeviceToHost);
	ofstream ply;
	char filename[500];
	sprintf(filename, "%s%s.ply", folder, file);
	ply.open(filename);
	if(!ply.is_open())
	{
		cout << "File not opened" << endl;
		return;
	}
	ply.precision(15);	
	ply.setf(ios::fixed);
	ply.setf(ios::showpoint);

	cout <<  filename << endl;
	ply << "ply \nformat ascii 1.0 \nelement vertex ";
    ply << nodesSize;
    ply << "\nproperty float x \nproperty float y \nproperty float z";
	ply << "\nproperty uchar red \nproperty uchar green \nproperty uchar blue";   
    ply << "\nelement face ";
    ply << surfaceSize << "\n";
    ply << "property list uchar int vertex_index\n";
    ply << "end_header\n";
	for(int i = 0; i < nodesSize; ++i)
	{
		ply << nodes[i * 3]; ply << " "; ply << nodes[i * 3 + 1]; ply << " ";ply << nodes[i * 3 + 2];ply << " "; 
		if(nodeProp[i] == 't' || nodeProp[i] == 's')
		{
			ply << 169; ply << " "; ply << 169; ply << " ";ply << 169;
		}
		else if(nodeProp[i] == 'f')
		{
			ply << 0; ply << " "; ply << 0; ply << " ";ply << 189;
		}
		else
		{
			ply << 189; ply << " "; ply << 0; ply << " ";ply << 0;
		}
		ply << endl;

	}
	for(int i = 0; i < surfaceSize; ++i)
	{
		ply << "3" << " " << surfaceTriangles[i * 3] << " " << surfaceTriangles[i * 3 + 1] << " " << surfaceTriangles[i * 3 + 2] << endl;
	}
	ply.close();	
}

float saveTheSurfaceTrimesh(char* folder, char *file)
{
	ofstream ply;
	char filename[500];
	sprintf(filename, "%s%s.ply", folder, file);
	TriMesh *m  = new TriMesh();
	float maxDiff = -1;
	float diff;
	for(int i = 0;i < nodesSize;++i)
	{
		m->vertices.push_back(vec(nodes[3 * i], nodes[3*i + 1], nodes[3 * i + 2]));
		//get difference between nodes and nodes_temp
		diff = (nodes[3 * i] - nodes_temp[3 * i]) * (nodes[3 * i] - nodes_temp[3 * i]);
		diff += (nodes[3 * i + 1] - nodes_temp[3 * i + 1]) * (nodes[3 * i + 1] - nodes_temp[3 * i + 1]);
		diff += (nodes[3 * i + 2] - nodes_temp[3 * i + 2]) * (nodes[3 * i + 2] - nodes_temp[3 * i + 2]);	
		if(diff > maxDiff) maxDiff = diff;	
		if(nodeProp[i] == 't')
		{
			m->colors.push_back(Color(153,255,153));			
		}
		else if(nodeProp[i] == 'f')
		{
			m->colors.push_back(Color(50,50,50));
		}
		else if(nodeProp[i] == 'm')
		{
			m->colors.push_back(Color(0,100,0));
		}
		else if(nodeProp[i] == 'b')
		{
			m->colors.push_back(Color(200,0,0));
		}
		else
		{
			m->colors.push_back(Color(201,201,201));
		}
	}
	for(int i = 0; i < surfaceSize; ++i)
	{
		m->faces.push_back(TriMesh::Face(surfaceTriangles[i * 3], surfaceTriangles[i * 3 + 1], surfaceTriangles[i * 3 + 2]));
	}
	m->write(filename);
	m->clear();
	return sqrt(maxDiff);
	
}

void saveTheSurfaceTrimeshInversed(char* folder, char *file)
{
	ofstream ply;
	char filename[500];
	sprintf(filename, "%s%s_inversed.ply", folder, file);
	TriMesh *m  = new TriMesh();
	for(int i = 0;i < nodesSize;++i)
	{
		
		m->vertices.push_back(vec(nodes[3 * i], -1 * nodes[3*i + 1] - 200, nodes[3 * i + 2]));
		if(nodeProp[i] == 't')
		{
			m->colors.push_back(Color(153,255,153));			
		}
		else if(nodeProp[i] == 'f')
		{
			m->colors.push_back(Color(50,50,50));
		}
		else if(nodeProp[i] == 'm')
		{
			m->colors.push_back(Color(0,100,0));
		}
		else if(nodeProp[i] == 'b')
		{
			m->colors.push_back(Color(200,0,0));
		}
		else
		{
			m->colors.push_back(Color(201,201,201));
		}
	}
	for(int i = 0; i < surfaceSize; ++i)
	{
		m->faces.push_back(TriMesh::Face(surfaceTriangles[i * 3], surfaceTriangles[i * 3 + 1], surfaceTriangles[i * 3 + 2]));
	}
	int lastNode = m->vertices.size();
	for(int i = 0;i < nodesSize;++i)
	{
		
		m->vertices.push_back(vec(nodes[3 * i], nodes[3*i + 1], nodes[3 * i + 2]));
		if(nodeProp[i] == 't')
		{
			m->colors.push_back(Color(153,255,153));			
		}
		else if(nodeProp[i] == 'f')
		{
			m->colors.push_back(Color(50,50,50));
		}
		else if(nodeProp[i] == 'm')
		{
			m->colors.push_back(Color(0,100,0));
		}
		else if(nodeProp[i] == 'b')
		{
			m->colors.push_back(Color(200,0,0));
		}
		else
		{
			m->colors.push_back(Color(201,201,201));
		}
	}
	for(int i = 0; i < surfaceSize; ++i)
	{
		m->faces.push_back(TriMesh::Face(lastNode + surfaceTriangles[i * 3], lastNode + surfaceTriangles[i * 3 + 1], lastNode + surfaceTriangles[i * 3 + 2]));
	}
	m->write(filename);
	m->clear();
	
}

float saveTheSurfaceTrimeshForces(char* folder, char *file, float maxValue)
{
	ofstream ply;
	char filename[500];
	sprintf(filename, "%s%s.ply", folder, file);
	TriMesh *m  = new TriMesh();
	float maxDiff = -1;
	float diff;
	for(int i = 0;i < nodesSize;++i)
	{
		m->vertices.push_back(vec(nodes[3 * i], nodes[3*i + 1], nodes[3 * i + 2]));
		//get difference between nodes and nodes_temp
		diff = (nodes[3 * i] - nodes_temp[3 * i]) * (nodes[3 * i] - nodes_temp[3 * i]);
		diff += (nodes[3 * i + 1] - nodes_temp[3 * i + 1]) * (nodes[3 * i + 1] - nodes_temp[3 * i + 1]);
		diff += (nodes[3 * i + 2] - nodes_temp[3 * i + 2]) * (nodes[3 * i + 2] - nodes_temp[3 * i + 2]);	
		if(diff > maxDiff) maxDiff = diff;
		float f = force[3 * i] * force[3 * i]	+ force[3 * i + 2] * force[3 * i + 2];
		if(nodeProp[i] != 'm') f += force[3 * i + 1] * force[3 * i + 1];
		f = f/maxValue;
		if(nodeProp[i] != 'f') m->colors.push_back(Color(0.8 + 0.2 * f, 0.8 * (1 - f),0.8 * (1 - f)));
		else m->colors.push_back(Color(50,50,50));
		
	}
	for(int i = 0; i < surfaceSize; ++i)
	{
		m->faces.push_back(TriMesh::Face(surfaceTriangles[i * 3], surfaceTriangles[i * 3 + 1], surfaceTriangles[i * 3 + 2]));
	}
	m->write(filename);
	m->clear();
	return sqrt(maxDiff);
	
}

void exportNodesAndTetras(char *folder, char *fileN)
{
	ofstream file;
	char filename[500];
	sprintf(filename, "%s%s.txt", folder, fileN);
	file.open(filename);
	file << nodesSize << endl;
	for(int i = 0; i < nodesSize; ++i)
	{
		file << nodes[3 * i] << " " << nodes[3 * i + 1] << " " << nodes[3 * i + 2] << endl;		
	}
	file << tetraSize << endl;
	for(int i = 0; i < tetraSize; ++i)
	{
		file << tetras[4 * i] << " " << tetras[4 * i + 1] << " " << tetras[4 * i + 2] << " " << tetras[4 * i + 3] <<  endl;
	}	
	file.close();
}

void exportNodesAndTetrasBinary(char *folder, char *fileN)
{
	char filename[500];
	sprintf(filename, "%s%s.bin", folder, fileN);
	ofstream file (filename, ios::out | ios::binary);
	file.write (reinterpret_cast<char*>(&nodesSize), sizeof(int));
	file.write(reinterpret_cast<char*>(nodes), sizeof(float) * nodesSize * 3);
	file.write (reinterpret_cast<char*>(&tetraSize), sizeof(int));
	file.write(reinterpret_cast<char*>(tetras), sizeof(unsigned int) * tetraSize * 4);
	file.write(reinterpret_cast<char*>(&averageSpacing), sizeof(float));
	file.write(reinterpret_cast<char*>(&surfaceSize), sizeof(int));		
	file.write(reinterpret_cast<char*>(surfaceTriangles), sizeof(unsigned int) * surfaceSize * 3);
	file.write(reinterpret_cast<char*>(nodeProp), sizeof(char) * nodesSize);
	file.close();
}

void copyToHost()
{
	cudaMemcpy(nodes, d_nodes, nodesSize*3*sizeof(float), cudaMemcpyDeviceToHost);
	cudaMemcpy(velocity, d_velocity, nodesSize*3*sizeof(float), cudaMemcpyDeviceToHost);
	cudaMemcpy(energy, d_volume, tetraSize*sizeof(float), cudaMemcpyDeviceToHost);
}

void setCudaGlobalParameters()
{
	cudaMemcpyToSymbol(parameters, parameter, 10 * sizeof(float));
}



void saveCurrentConfiguration(char *folder, char *fileN, bool tempValues)
{
	char filename[1000];
	sprintf(filename, "%s%s.bin", folder, fileN);
	ofstream file (filename, ios::out | ios::binary);
	//nodes
	file.write (reinterpret_cast<char*>(&nodesSize), sizeof(int));
	if(tempValues)
	{
		file.write(reinterpret_cast<char*>(nodes_temp), sizeof(float) * nodesSize * 3);	
	}
	else
	{
		file.write(reinterpret_cast<char*>(nodes), sizeof(float) * nodesSize * 3);	
	}
	//tetras	
	file.write (reinterpret_cast<char*>(&tetraSize), sizeof(int));
	file.write(reinterpret_cast<char*>(tetras), sizeof(int) * tetraSize * 4);
	//A0s
	float *A0s = (float*)malloc(tetraSize*9*sizeof(float));
	cudaMemcpy(A0s, d_A0s, tetraSize*9*sizeof(float), cudaMemcpyDeviceToHost);	
	file.write(reinterpret_cast<char*>(A0s), tetraSize*9*sizeof(float));
	free(A0s);
	//directions
	bool *directions = (bool*)malloc(tetraSize*4*sizeof(bool));
	cudaMemcpy(directions, d_directions, tetraSize*4*sizeof(bool), cudaMemcpyDeviceToHost);			
	file.write(reinterpret_cast<char*>(directions), tetraSize*4*sizeof(bool));
	free(directions);
	//bulk
	float *temp = (float*)malloc(tetraSize * sizeof(float));
	cudaMemcpy(temp, d_bulk, tetraSize*sizeof(float), cudaMemcpyDeviceToHost);	
	file.write(reinterpret_cast<char*>(temp), tetraSize*sizeof(float));	
	//shear
	cudaMemcpy(temp, d_shear, tetraSize*sizeof(float), cudaMemcpyDeviceToHost);	
	file.write(reinterpret_cast<char*>(temp), tetraSize*sizeof(float));
	free(temp);
	//velocity
	if(tempValues)
	{		
		file.write(reinterpret_cast<char*>(velocity_temp), nodesSize*3*sizeof(float));
	}
	else
	{
		file.write(reinterpret_cast<char*>(velocity), nodesSize*3*sizeof(float));	
	}
	//averageSpacing
	file.write(reinterpret_cast<char*>(&averageSpacing), sizeof(float));
	//surface
	file.write(reinterpret_cast<char*>(&surfaceSize), sizeof(int));		
	file.write(reinterpret_cast<char*>(surfaceTriangles), sizeof(unsigned int) * surfaceSize * 3);
	//nodeProp
	file.write(reinterpret_cast<char*>(nodeProp), sizeof(char) * nodesSize);
	//parameters
	file.write(reinterpret_cast<char*>(parameter), sizeof(float) * 10);
	file.write(reinterpret_cast<char*>(constrainedPlaneParameters), sizeof(float) * 7);
	//growth directions	
	float *gd = (float*)malloc(tetraSize*3*sizeof(float));
	cudaMemcpy(gd, d_growth_directions, tetraSize*3*sizeof(float), cudaMemcpyDeviceToHost);			
	file.write(reinterpret_cast<char*>(gd), tetraSize*3*sizeof(float));
	free(gd);
	//growth
	float *gr = (float*)malloc(tetraSize*sizeof(float));
	cudaMemcpy(gr, d_growth, tetraSize*sizeof(float), cudaMemcpyDeviceToHost);			
	file.write(reinterpret_cast<char*>(gr), tetraSize*sizeof(float));
	free(gr);
	file.write(reinterpret_cast<char*>(volume), tetraSize*sizeof(float));	
	file.close();
	cout << "Configuration saved to: " << filename << endl;
}
