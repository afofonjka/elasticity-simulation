CUDA_PATH       ?= /usr/bin

GCC ?= g++

NVCC := $(CUDA_PATH)/nvcc

# Common includes and paths for CUDA
INCLUDES  := -I ./ann_1.1.2/include -I ./trimesh2/include -I /usr/lib/cuda/include
LIBRARIES := -L ./ann_1.1.2/lib -L ./trimesh2/lib.Linux64 -L /usr/lib/cuda/lib64
################################################################################

LIBS := -lANN -lgomp -ltrimesh -lpthread

CUDAFLAGS := -arch=sm_35 --prec-div=true --prec-sqrt=true --fmad=false

################################################################################

all: build

build: cuda_simulation

kernel.o: kernel.cu
	$(NVCC)  $(CUDAFLAGS) -c $(INCLUDES) $(LIBRARIES) $(LIBS) kernel.cu -o kernel.o
cuda_simulation: kernel.o
	$(NVCC) cuda_simulation.cu $(LIBRARIES) $(INCLUDES) $(LIBS) -o cuda_simulation

run: build
	$(EXEC) ./cuda_simulation

clean:
	rm -f cuda_simulation kernel.o

