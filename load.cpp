#include <fstream>
#include <iostream>
#include <string>
#include <iomanip> 
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <pthread.h>
#include <queue>

#include <ANN/ANN.h>

#include "kernel.cu"

using namespace std;
using namespace trimesh;


float distance(float x1, float y1, float z1,float x2, float y2, float z2)
{
	return sqrt((x1 - x2) * (x1 - x2) + (y1 - y2)*(y1 - y2) + (z1 - z2)*(z1 - z2));
}

float distanceToPlane(float x0, float y0, float z0, float a, float b, float c, float d)
{
	return abs(a * x0 + b * y0 + c * z0 + d)/sqrt(a*a + b*b + c*c);
}


float getAverageSpacing(int n1, int n2, int n3, int n4)
{
	float a = distance(nodes[3 * n1], nodes[3 * n1 + 1], nodes[3 * n1 + 2], nodes[3 * n2], nodes[3 * n2 + 1], nodes[3 * n2 + 2]);
	a += distance(nodes[3 * n1], nodes[3 * n1 + 1], nodes[3 * n1 + 2], nodes[3 * n3], nodes[3 * n3 + 1], nodes[3 * n3 + 2]);
	a += distance(nodes[3 * n1], nodes[3 * n1 + 1], nodes[3 * n1 + 2], nodes[3 * n4], nodes[3 * n4 + 1], nodes[3 * n4 + 2]);
	a += distance(nodes[3 * n2], nodes[3 * n2 + 1], nodes[3 * n2 + 2], nodes[3 * n3], nodes[3 * n3 + 1], nodes[3 * n3 + 2]);
	a += distance(nodes[3 * n2], nodes[3 * n2 + 1], nodes[3 * n2 + 2], nodes[3 * n4], nodes[3 * n4 + 1], nodes[3 * n4 + 2]);
	a += distance(nodes[3 * n3], nodes[3 * n3 + 1], nodes[3 * n3 + 2], nodes[3 * n4], nodes[3 * n4 + 1], nodes[3 * n4 + 2]);
	return a/6;
}



void distanceToGrowthSurfaceAndNormalsTrimesh(const char *filename)
{
	//load the surface
	TriMesh *m = TriMesh::read(filename);
	if(!m) 
	{
		cout << "Growth mesh file does not exist" << endl;
		return;
	}
	growthSurfaceSize = m->vertices.size();			
	ANNpointArray dataPts = annAllocPts(growthSurfaceSize, 3);
	ANNkd_tree*	kdTree;
	normals = (float*)malloc(growthSurfaceSize * 3 * sizeof(float));
	for(int i = 0; i < growthSurfaceSize; ++i)
	{
		dataPts[i][0] = m->vertices[i][0];
		dataPts[i][1] = m->vertices[i][1];
		dataPts[i][2] = m->vertices[i][2];
		normals[3 * i] = m->normals[i][0];
		normals[3 * i + 1] = m->normals[i][1];
		normals[3 * i + 2] = m->normals[i][2];
	}
	kdTree = new ANNkd_tree(dataPts,growthSurfaceSize,3);
	growth = (float*)malloc(tetraSize * sizeof(float));
	closestPoint = (unsigned int*)malloc(tetraSize * sizeof(unsigned int));
	ANNpoint queryPt = annAllocPt(3);
	ANNidxArray	nnIdx = new ANNidx[1];
	ANNdistArray dists = new ANNdist[1];	
	for(int i = 0; i < tetraSize; ++i)
	{
		queryPt[0] = (nodes[3 * tetras[4 * i]] + nodes[3 * tetras[4 * i + 1]] + nodes[3 * tetras[4 * i + 2]] + nodes[3 * tetras[4 * i + 3]])/4;
		queryPt[1] = (nodes[3 * tetras[4 * i] + 1] + nodes[3 * tetras[4 * i + 1] + 1] + nodes[3 * tetras[4 * i + 2] + 1] + nodes[3 * tetras[4 * i + 3] + 1])/4;
		queryPt[2] = (nodes[3 * tetras[4 * i] + 2] + nodes[3 * tetras[4 * i + 1] + 2] + nodes[3 * tetras[4 * i + 2] + 2] + nodes[3 * tetras[4 * i + 3] + 2])/4;
		kdTree->annkSearch(queryPt,	1,nnIdx,dists,0);
		closestPoint[i] = nnIdx[0];
		growth[i] = sqrt(dists[0]);
	}
	delete [] nnIdx;							
    delete [] dists;
    delete [] dataPts;
    delete kdTree;
    m->clear();
}

void distanceToFixedSurfaceTrimesh(const char *filename, float T)
{
	TriMesh *m = TriMesh::read(filename);
	if(!m) 
	{
		cout << "Fixed mesh file does not exist" << endl;
		return;
	}
	int n = m->vertices.size();
	ANNpointArray dataPts = annAllocPts(n, 3);
	ANNkd_tree*	kdTree;
	for(int i = 0; i < n; ++i)
	{
		dataPts[i][0] = m->vertices[i][0];
		dataPts[i][1] = m->vertices[i][1];
		dataPts[i][2] = m->vertices[i][2];

	}
	kdTree = new ANNkd_tree(dataPts,n,3);	
	ANNpoint queryPt = annAllocPt(3);
	ANNidxArray	nnIdx = new ANNidx[1];
	ANNdistArray dists = new ANNdist[1];	
	for(int i = 0; i < nodesSize; ++i)
	{
		queryPt[0] = nodes[3 * i];
		queryPt[1] = nodes[3 * i + 1];
		queryPt[2] = nodes[3 * i + 2];
		kdTree->annkSearch(queryPt,	1,nnIdx,dists,0);
		if(sqrt(dists[0]) <= T)
		{
			nodeProp[i] = 'f';
		}
	}
	delete [] nnIdx;							// clean things up
    delete [] dists;
    delete [] dataPts;
    delete kdTree;
    m->clear();
}

//constrained plane is described with (a,b,c,d) ax + by + cz + d = 0
//if distance to the plane is less than epsilon we set NodeProp to 'm'
void getConstrainedNodes()
{
	for(int i = 0; i < nodesSize; ++i)
	{
		if(distanceToPlane(nodes[3 * i], nodes[3 * i + 1], nodes[3 * i + 2], constrainedPlaneParameters[0], constrainedPlaneParameters[1], constrainedPlaneParameters[2],constrainedPlaneParameters[3]) < cse) nodeProp[i] = 'm';
		
	}
}


bool loadMeshTetgen(const char* filename, float T)
{
	//reading all nodes
	char file[1000];
	sprintf(file,"%s.node", filename);
	fstream f;
	f.open(file);
	vector<int> sNodes;
	if(!f.is_open())
	{
		cout << "Node file doesn't exist" << endl;
		return false;
	}
	int temp;
	//loading number of nodes
	f >> nodesSize;
	f >> temp;	f >> temp;	f >> temp;
	nodes = (float*)malloc(nodesSize * 3 * sizeof(float));
	nodeProp =  (char*)malloc(nodesSize * sizeof(char));
	for(int i = 0; i < nodesSize; ++i)
	{
		f >> temp;
		f >> nodes[3 * (temp - 1)];
		f >> nodes[3 * (temp - 1) + 1];
		f >> nodes[3 * (temp - 1) + 2];
		nodeProp[i] = 'n';
		//check if node is on the surface and add it to a vector
	}
	sprintf(file,"%s.face", filename);
	f.close();
	f.open(file);
	if(!f.is_open())
	{
		cout << "Face file doesn't exist" << endl;
		return false;
	}
	f >> surfaceSize;
	f >> temp;
	surfaceTriangles = (unsigned int*)malloc(surfaceSize * 3 * sizeof(unsigned int));
	for(int i = 0; i < surfaceSize; ++i)
	{
		f >> temp;
		f >> surfaceTriangles[i * 3];
		f >> surfaceTriangles[i * 3 + 1];
		f >> surfaceTriangles[i * 3 + 2];
		f >> temp;
		surfaceTriangles[i * 3]--;
		surfaceTriangles[i * 3 + 1]--;
		surfaceTriangles[i * 3 + 2]--;
		if(nodeProp[surfaceTriangles[i * 3]] == 'n')
		{
			sNodes.push_back(surfaceTriangles[i * 3]);
			nodeProp[surfaceTriangles[i * 3]] = 's';
		}
		if(nodeProp[surfaceTriangles[i * 3 + 1]] == 'n')
		{
			sNodes.push_back(surfaceTriangles[i * 3 + 1]);
			nodeProp[surfaceTriangles[i * 3 + 1]] = 's';
		}
		if(nodeProp[surfaceTriangles[i * 3 + 2]] == 'n')
		{
			sNodes.push_back(surfaceTriangles[i * 3 + 2]);
			nodeProp[surfaceTriangles[i * 3 + 2]] = 's';;
		}
	}
	sprintf(file,"%s.ele", filename);
	f.close();
	f.open(file);
	if(!f.is_open())
	{
		cout << "Element file doesn't exist" << endl;
		return false;
	}
	f >> tetraSize;
	f >> temp; f >> temp;
	tetras = (unsigned int*)malloc(tetraSize * 4 * sizeof(unsigned int));
	averageSpacing = 0;
	for(int i = 0; i < tetraSize; ++i)
	{
		f >> temp;
		f >> tetras[4 * i];
		f >> tetras[4 * i + 1];
		f >> tetras[4 * i + 2];
		f >> tetras[4 * i + 3];
		tetras[4 * i]--;
		tetras[4 * i + 1]--;
		tetras[4 * i + 2]--;
		tetras[4 * i + 3]--;
		if( tetras[4 * i] >= nodesSize) cout << tetras[4 * i] << " " <<nodesSize << endl;
		if( tetras[4 * i + 1] >= nodesSize) cout << tetras[4 * i + 1] << " " <<nodesSize << endl;
		if( tetras[4 * i + 2] >= nodesSize) cout << tetras[4 * i + 2] << " " <<nodesSize << endl;
		if( tetras[4 * i + 3] >= nodesSize) cout << tetras[4 * i + 3] << " " <<nodesSize << endl;
		averageSpacing += getAverageSpacing(tetras[4 * i], tetras[4 * i + 1], tetras[4 * i + 2], tetras[4 * i + 3]);
	}
	averageSpacing /= tetraSize;
	surfaceNodes = (unsigned int*)malloc(sNodes.size() * sizeof(unsigned int));
	ANNpointArray		dataPts;
	ANNkd_tree*			kdTree;
	dataPts = annAllocPts(sNodes.size(), 3);
	for(int i = 0; i < sNodes.size(); ++i)
	{
		surfaceNodes[i] = sNodes[i]; 
		dataPts[i][0] = nodes[sNodes[i] * 3];
		dataPts[i][1] = nodes[sNodes[i] * 3 + 1];
		dataPts[i][2] = nodes[sNodes[i] * 3 + 2];
	}
	kdTree = new ANNkd_tree(dataPts,sNodes.size(),3);
	surfaceNodesSize = sNodes.size(); 
	sNodes.clear();
	ANNpoint queryPt = annAllocPt(3);
	ANNidxArray	nnIdx = new ANNidx[1];
	ANNdistArray dists = new ANNdist[1];	
	for(int i = 0; i < nodesSize; ++i)
	{
		if(nodeProp[i] == 'n')
		{
			queryPt[0] = nodes[3 * i];
			queryPt[1] = nodes[3 * i + 1];
			queryPt[2] = nodes[3 * i + 2];
			kdTree->annkSearch(queryPt,	1,nnIdx,dists,0);	
			if(sqrt(dists[0]) <= T)
			{
				nodeProp[i] = 't';
			}
			else if(sqrt(dists[0]) > T*fixedFactor)
			{
				nodeProp[i] = 'f';
			}
			else
			{
				nodeProp[i] = 'b';
			}
		}
	}
	cout << "Number of nodes: " << nodesSize << endl;
	cout << "Number of tetrahedrons: " << tetraSize << endl;
	cout << "Number of surface triangles: " << surfaceSize << endl;
	cout << "Average mesh spacing: " << averageSpacing << endl;	
	cout << "Number of surface nodes:" << surfaceNodesSize << endl;
	delete [] nnIdx;							// clean things up
    delete [] dists;
    delete [] dataPts;
    delete kdTree;
	return true;
    		
}


bool loadMeshTxt(char* filename)
{
	fstream f;
	f.open(filename);
	if(!f.is_open())
	{
		cout << "File doesn't exist" << endl;
		return false;
	}
	int size;
	f >> size;
	nodes = (float*)malloc(size * 3 * sizeof(float));
	for(int i = 0; i < size; ++i)
	{
		f >> nodes[3 * i];
		f >> nodes[3 * i + 1];
		f >> nodes[3 * i + 2];
	}
	cout << "Number of nodes: " << size << endl;
	nodesSize = size;
	f >> size;
	tetras = (unsigned int*)malloc(size * 4 * sizeof(unsigned int));
	averageSpacing = 0;
	for(int i = 0; i < size; ++i)
	{
		f >> tetras[4 * i];
		f >> tetras[4 * i + 1];
		f >> tetras[4 * i + 2];
		f >> tetras[4 * i + 3];
		averageSpacing += getAverageSpacing(tetras[4 * i], tetras[4 * i + 1], tetras[4 * i + 2], tetras[4 * i + 3]);
	}
	tetraSize = size;
	cout << "Number of tetrahedrons: " << size << endl;
	averageSpacing /= size;
	cout << "Average mesh spacing: " << averageSpacing << endl;
	return true;
}

bool loadParametersForNewSimulation(char *filename)
{
	fstream f;
	f.open(filename);
	if(!f.is_open())
	{
		cout << "File doesn't exist" << endl;
		return false;
	}	
	f >> saveFolder;
	cout << "Save folder " << saveFolder << endl;
	f >> meshFile;
	cout << "Mesh file " << meshFile << endl;
	f >> BFS;
	f >> growthSurface;
	if(BFS) cout << "Optimizing mesh structure using BFS" << endl;
	cout << "Growth surface: " << growthSurface << endl;
	f >> fixedSurface; 
	cout << "Fixed surface: " << fixedSurface << endl;
	f >> skin;
	cout << "Skin thickness: " << skin << endl;
	f >> tangentialGrowth;
	cout << "Growth: " << tangentialGrowth << endl;
	f >> middleGrowth;
	cout << "Middle growth: " << middleGrowth << endl;
	f >> shears[0] >> shears[1];
	cout << "Shear moduli top and bottom: " << shears[0] << ", " << shears[1] << endl;
	f >> bulks[0] >> bulks[1];
	cout << "Bulk moduli top and bottom: " << bulks[0] << ", " << bulks[1] << endl;
	f >> saveFrequency;
	cout << "Save frequency: " << saveFrequency << endl;
	f >> stopError;
	cout << "Stop error: " << stopError << endl;
	f >> displTol;
	cout << "Displacement tolerance: " << displTol << endl;
	f >> dampingFactor;
	cout << "Damping factor: " << dampingFactor << endl;
	f >> timeStepFactor;
	cout << "Time step factor: " << timeStepFactor << endl;
	f >> fixedFactor;
	cout << "Fixed factor: " << fixedFactor << endl;
	f >> constrainedPlaneParameters[0] >> constrainedPlaneParameters[1] >> constrainedPlaneParameters[2] >> constrainedPlaneParameters[3] >> cse;
	cout << "Constrained distance: " << cse << endl;
	f >> constrainedPlaneParameters[4] >> constrainedPlaneParameters[5] >> constrainedPlaneParameters[6]; 
	cout << "Constrained plane parameters: " << endl;
	for(int i = 0; i < 7; ++i) cout << constrainedPlaneParameters[i] << " ";
	cout << endl;
	return true;
}

bool loadParametersForOldSimulation(char *filename)
{
	fstream f;
	f.open(filename);
	if(!f.is_open())
	{
		cout << "File doesn't exist" << endl;
		return false;
	}
	f >> saveFolder;
	cout << "Save folder: " << saveFolder << endl;	
	f >> binaryFile;
	cout << "Binary file: " << binaryFile << endl;
	f >> growthAdd;
	cout << "Growth to add: " << growthAdd << endl;
	f >> middleGrowthAdd;
	cout << "Middle growth to add: " << middleGrowthAdd << endl;
       	f >> shears[0] >> shears[1];
        cout << "Shear moduli top and bottom: " << shears[0] << ", " << shears[1] << endl;
        f >> bulks[0] >> bulks[1];
        cout << "Bulk moduli top and bottom: " << bulks[0] << ", " << bulks[1] << endl;
	f >> saveFrequency;
	cout << "Save frequency: " << saveFrequency << endl;
	f >> stopError;
	cout << "Stop error: " << stopError << endl;
	f >> displTol;
	cout << "Displacement tolerance: " << displTol << endl;
	f >> dampingFactor;
	cout << "Damping factor: " << dampingFactor << endl;
	f >> timeStepFactor;
	cout << "Time step factor: " << timeStepFactor << endl;
	return true;
}

bool loadMeshBinary(const char* filename)
{
	cout << "Reading binary file: " << filename << endl;
	fstream file(filename, ios::in | ios::binary);
	if(!file)
	{
		cout << "No bin file to open" << endl;
		return false;
	}
	file.read((char*)&nodesSize, sizeof(int));
	nodes = (float*)malloc(nodesSize * 3 * sizeof(float));
	cout << "Number of nodes: " << nodesSize << endl;
	file.read ((char*)nodes, 3 * nodesSize * sizeof(float));
	file.read ((char*)&tetraSize, sizeof(int));
	tetras = (unsigned int*)malloc(tetraSize * 4 * sizeof(unsigned int));
	file.read((char*)tetras, tetraSize * 4 * sizeof(unsigned int));
	cout << "Number of tetrahedrons: " << tetraSize << endl;
	file.read ((char*)&averageSpacing, sizeof(float));
	cout << "Average mesh spacing: " << averageSpacing << endl;
	file.read ((char*)&surfaceSize, sizeof(int));
	surfaceTriangles = (unsigned int*)malloc(surfaceSize * 3 * sizeof(unsigned int));
	file.read((char*)surfaceTriangles, surfaceSize * 3 * sizeof(unsigned int));
	cout << "Surface Size: " << surfaceSize << endl;
	nodeProp = (char*)malloc(nodesSize * sizeof(char));
	file.read((char*)nodeProp, nodesSize * sizeof(char));
	file.close();
	return true;
}

void cleanFixedPart()
{
	map<int, int> indexMapping;
	vector<int> newTetras;
	vector<int> newSurfaceTri;
	vector<char> newNodeProp;
	for(int i = 0; i < tetraSize; ++i)
	{
		if(nodeProp[tetras[i * 4]] != 'f' || nodeProp[tetras[i * 4 + 1]] != 'f' || nodeProp[tetras[i * 4 + 2]] != 'f' || nodeProp[tetras[i * 4 + 3]] != 'f')
		{
			newTetras.push_back(i);
			if(indexMapping.count(tetras[i * 4]) == 0)
			{
				newNodeProp.push_back(nodeProp[tetras[i * 4]]);
				indexMapping[tetras[i * 4]] = newNodeProp.size() - 1;
			}
			if(indexMapping.count(tetras[i * 4 + 1]) == 0)
			{
				newNodeProp.push_back(nodeProp[tetras[i * 4 + 1]]);
				indexMapping[tetras[i * 4 + 1]] = newNodeProp.size() - 1;
			}
			if(indexMapping.count(tetras[i * 4 + 2]) == 0)
			{
				newNodeProp.push_back(nodeProp[tetras[i * 4 + 2]]);
				indexMapping[tetras[i * 4 + 2]] = newNodeProp.size() - 1;
			}
			if(indexMapping.count(tetras[i * 4 + 3]) == 0)
			{
				newNodeProp.push_back(nodeProp[tetras[i * 4 + 3]]);
				indexMapping[tetras[i * 4 + 3]] = newNodeProp.size() - 1;
			}
		}
	}
	for(int i = 0; i < surfaceSize; ++i)
	{
		newSurfaceTri.push_back(i);
		if(indexMapping.count(surfaceTriangles[3 * i]) == 0)
		{
			newNodeProp.push_back(nodeProp[surfaceTriangles[3 * i]]);
			indexMapping[surfaceTriangles[3 * i]] = newNodeProp.size() - 1;
		}
		if(indexMapping.count(surfaceTriangles[3 * i + 1]) == 0)
		{
			newNodeProp.push_back(nodeProp[surfaceTriangles[3 * i + 1]]);
			indexMapping[surfaceTriangles[3 * i + 1]] = newNodeProp.size() - 1;
		}
		if(indexMapping.count(surfaceTriangles[3 * i + 2]) == 0)
		{
			newNodeProp.push_back(nodeProp[surfaceTriangles[3 * i + 2]]);
			indexMapping[surfaceTriangles[3 * i + 2]] = newNodeProp.size() - 1;
		}
	}
	//create new arrays
	unsigned int *tetras1 = (unsigned int*)malloc(newTetras.size() * 4 * sizeof(unsigned int)); 
	unsigned int *surfaceTriangles1 = (unsigned int*)malloc(newSurfaceTri.size() * 3 * sizeof(unsigned int));
	char *nodeProp1 = (char*)malloc(newNodeProp.size() *  sizeof(char));
	float *nodes1 = (float*)malloc(newNodeProp.size() * 3 *  sizeof(float));
	for(int i = 0; i < newTetras.size(); ++i)
	{
		tetras1[4 * i] = indexMapping[tetras[4 * newTetras[i]]];
		tetras1[4 * i + 1] = indexMapping[tetras[4 * newTetras[i] + 1]];
		tetras1[4 * i + 2] = indexMapping[tetras[4 * newTetras[i] + 2]];
		tetras1[4 * i + 3] = indexMapping[tetras[4 * newTetras[i] + 3]];
	}
	for(int i = 0; i < newSurfaceTri.size(); ++i)
	{
		surfaceTriangles1[3 * i] = indexMapping[surfaceTriangles[3 * newSurfaceTri[i]]];
		surfaceTriangles1[3 * i + 1] = indexMapping[surfaceTriangles[3 * newSurfaceTri[i] + 1]];
		surfaceTriangles1[3 * i + 2] = indexMapping[surfaceTriangles[3 * newSurfaceTri[i] + 2]];
	}
	for(int i = 0; i < newNodeProp.size(); ++i)
	{
		nodeProp1[i] = newNodeProp[i];
	}
	int newNodes = 0;
	for(int i = 0; i < nodesSize; ++i)
	{
		if(indexMapping.count(i) > 0)
		{
			nodes1[3 * indexMapping[i]] = nodes[3 * i];
			nodes1[3 * indexMapping[i] + 1] = nodes[3 * i + 1];
			nodes1[3 * indexMapping[i] + 2] = nodes[3 * i + 2];
			newNodes++;
		}
	}
	cout << "Nodes size: " << nodesSize << " ----> " << newNodeProp.size() << " " << newNodes << endl;
	nodesSize = newNodeProp.size();
	cout << "Tetra size: " << tetraSize << " ----> " << newTetras.size() << " "  << endl;
	tetraSize = newTetras.size();
	cout << "Surface size: " << surfaceSize << " ----> " << newSurfaceTri.size() << " "  << endl;
	surfaceSize = newSurfaceTri.size();
	free(nodes);
	free(tetras);
	free(surfaceTriangles);
	free(nodeProp);
	newNodeProp.clear();
	newTetras.clear();
	newSurfaceTri.clear();
	indexMapping.clear();
	nodes = nodes1;
	tetras = tetras1;
	surfaceTriangles = surfaceTriangles1;
	nodeProp = nodeProp1; 
	
	
}

//change fixed node properties to the ones dominating the tetrahedron
void adjustFixedNodeProperties()
{
	for(int i = 0; i < tetraSize; ++i)
	{
		if(nodeProp[tetras[i * 4]] == 'f' || nodeProp[tetras[i * 4 + 1]] == 'f' || nodeProp[tetras[i * 4 + 2]] == 'f' || nodeProp[tetras[i * 4 + 3]] == 'f')
		{
			int topNodes = 0;
			int bottomNodes = 0;
			char newProp;
			for(int j = 0; j < 4; ++j)
			{
				if(nodeProp[tetras[i * 4 + j]] == 't' || nodeProp[tetras[i * 4 + j]] == 's') topNodes++;
				else bottomNodes++;
			}
			if(topNodes > bottomNodes) newProp = 't';
			else newProp = 'b'; 
			for(int j = 0; j < 4; ++j)
			{
				if(nodeProp[tetras[i * 4 + j]] == 'f') nodeProp[tetras[i * 4 + j]] = newProp;
			}			
			
		}
				
	}
}

void getSurfaceTriangles()
{
	int number = tetraSize * 4;
	surfaceTriangles = (unsigned int*)malloc(number * 3 * sizeof(unsigned int));
	for(int i = 0; i < tetraSize; ++i)
	{
		//0, 1, 2
		surfaceTriangles[12 * i] = tetras[4 * i];
		surfaceTriangles[12 * i + 1] = tetras[4 * i + 1];
		surfaceTriangles[12 * i + 2] = tetras[4 * i + 2];
		//0, 1, 3
		surfaceTriangles[12 * i + 3] = tetras[4 * i];
		surfaceTriangles[12 * i + 4] = tetras[4 * i + 1];
		surfaceTriangles[12 * i + 5] = tetras[4 * i + 3];
		//0, 2, 3
		surfaceTriangles[12 * i + 6] = tetras[4 * i];
		surfaceTriangles[12 * i + 7] = tetras[4 * i + 2];
		surfaceTriangles[12 * i + 8] = tetras[4 * i + 3];
		//1, 2, 3
		surfaceTriangles[12 * i + 9] = tetras[4 * i + 1];
		surfaceTriangles[12 * i + 10] = tetras[4 * i + 2];
		surfaceTriangles[12 * i + 11] = tetras[4 * i + 3];
	}
}

void sortBFS(const char *filename, int first)
{
	char file[1000];
	sprintf(file,"%s.neigh", filename);
	fstream f;
	f.open(file);
	if(!f.is_open())
	{
		cout << "Node file doesn't exist" << endl;
		return;
	}
	int *neigh = (int*)malloc(4 * tetraSize * sizeof(int));
	int temp;
	f >> temp;
	f >> temp;
	for(int i = 0; i < tetraSize; ++i)
	{
		f >> temp;
		for(int j = 0; j < 4; ++j)
		{
			f >> neigh[4 * i + j];
			neigh[4 * i + j]--;
		}
	}
	map<int,int> tInd;
	map<int,int> nInd;
	map<int, bool> added;
	queue<int> a;
	added[first] = true;
	a.push(first);
	int tCount = 0;
	int nCount = 0;
	while(!a.empty())
	{
		temp = a.front();
		a.pop();
		if(tInd.find(temp) == tInd.end())
		{
			tInd[temp] = tCount;
			tCount++;
		}
		
		for(int j = 0; j < 4; ++j)
		{
			if(tInd.find(neigh[4 * temp + j]) == tInd.end() && neigh[4 * temp + j] >= 0 && added.find(neigh[4 * temp + j]) == added.end())
			{
				a.push(neigh[4 * temp + j]);
				added[neigh[4 * temp + j]] = true;
			}
			if(nInd.find(tetras[4 * temp + j]) == nInd.end())
			{
				nInd[tetras[4 * temp + j]] = nCount;
				nCount++;
			}
		}		
	}
	if(tCount == tetraSize && nCount == nodesSize)
	{
		float * nodes1 = (float*)malloc(3 * nodesSize * sizeof(float));
		unsigned int *tetras1 = (unsigned int*)malloc(4 * tetraSize * sizeof(unsigned int));
		unsigned int *surfaceTriangles1 = (unsigned int*)malloc(3 * surfaceSize * sizeof(unsigned int));
		char *nodeProp1 = (char*)malloc(nodesSize * sizeof(char));
		for(int i = 0; i < tetraSize; ++i)
		{
			for(int j = 0; j < 4; ++j)
			{
				tetras1[4 * tInd[i] + j] = nInd[tetras[4 * i + j]];
			}
		}
		for(int i = 0; i < surfaceSize; ++i)
		{
			for(int j = 0; j < 3; ++j)
			{
				surfaceTriangles1[3 * i + j] = nInd[surfaceTriangles[3 * i + j]];
			}
		}
		for(int i = 0; i < nodesSize; ++i)
		{
			for(int j = 0; j < 3; ++j)
			{
				nodes1[3 * nInd[i] + j] = nodes[3 * i + j];
			}
		}
		for(int i = 0; i < nodesSize; ++i)
		{
			for(int j = 0; j < 3; ++j)
			{
				nodes1[3 * nInd[i] + j] = nodes[3 * i + j];
			}
			nodeProp1[nInd[i]] = nodeProp[i];
		}
		free(nodes);
		free(tetras);
		free(surfaceTriangles);
		free(nodeProp);
		surfaceTriangles = surfaceTriangles1;
		nodes = nodes1;
		tetras = tetras1;
		nodeProp = nodeProp1;
	}	
}
