#include "load.cpp"

pthread_barrier_t youCanCopyBarrier, youCanSaveBarrier;


using namespace std;
using namespace trimesh;


float getMaximumForce()
{
	float max = -100000;
	for(int i = 0; i < nodesSize; ++i)
	{
		if(nodeProp[i] != 'f')
		{
			float f = force[3 * i] * force[3 * i] + force[3 * i + 2] * force[3 * i + 2];
			if(nodeProp[i] != 'm') f += force[3 * i + 1] * force[3 * i + 1];
			if(f > max) max = f;
		}
	}
	return max;
}

void calculateEnergyOfTheSystem(double *e, double *no_fixed)
{
	for(int i = 0; i < tetraSize; ++i)
	{
		double temp = energy[i] * volume[i];
		if(nodeProp[tetras[4 * i]] != 'f' && nodeProp[tetras[4 * i + 1]] != 'f' && nodeProp[tetras[4 * i + 2]] != 'f' && nodeProp[tetras[4 * i + 3]] != 'f') (*no_fixed) += temp;
		(*e) += temp;
	}
}


void *savePoints(void *value)
{
	int counter = 0;
	int savedFiles = 0;
	while(!stop)
	{
		pthread_barrier_wait(&youCanSaveBarrier);
		char saveFileTemp[1000];
		sprintf(saveFileTemp, "%d", savingit);		
		float max_displacement = saveTheSurfaceTrimesh(simulationStepsFolder, saveFileTemp)/saveFrequency;
		//saveTheSurfaceTrimeshInversed(simulationStepsFolder, saveFileTemp);
		double e = 0;
		double no_fixed = 0;
		calculateEnergyOfTheSystem(&e, &no_fixed);
		//float max = getMaximumForce();
		//sprintf(saveFileTempForce, "forces/%d_force_%d", savingit, int(max));
		//saveTheSurfaceTrimeshForces(simulationStepsFolder, saveFileTempForce, max);
		
		if(abs(e - lastEnergy) < parameter[0] * lastEnergy)
		{
			counter++;
		}
		else
		{
			counter = 0;
		}
		cout << "********************************" << endl;
		cout << "Max displacement/save_frequency = " << max_displacement << endl;
		cout << "Total energy = " << e << endl;
		cout << "Not fixed total energy = " << no_fixed << endl;
		cout << "Counter = " << counter << " / " << stopCounter <<endl;
		cout << "Stop parameter: " << parameter[0] * lastEnergy <<endl;
		cout << "********************************" << endl;
		if(max_displacement != max_displacement || e != e)
		{
			cout << "Simulation error" << endl;
			saveCurrentConfiguration(simulationStepsFolder,(char *)"last_itaration", true);
			stop = true;
		}	
		else if(counter >= stopCounter && (max_displacement < displTol || e == lastEnergy))
		{
			stop = true;
			saveCurrentConfiguration(simulationStepsFolder,(char *) "last_iteration", false);
			//save the inversed configuration
			saveTheSurfaceTrimeshInversed(simulationStepsFolder, saveFileTemp);
		}
		else
		{
			//copy nodes to nodes temp and velocity to velocity temp because everything went ok
			saveCurrentConfiguration(simulationStepsFolder,saveFileTemp, false);
			for(int i = 0; i < 3 * nodesSize; ++i)
			{
				nodes_temp[i] = nodes[i];
				velocity_temp[i] = velocity[i];
			}
		}
		lastEnergy = e;
		savedFiles++;
		pthread_barrier_wait(&youCanCopyBarrier);		
	}
	return NULL;
}



int main(int argc, char **argv)
{
	if(argc < 3)
	{
		cout << "Command line arguments:" << endl;
		cout << "1. -new or -old" << endl;
		cout << "2. Options file (see README for additional instructions)" << endl;
		exit(-1);
	}
	bool binary = false;
	if(strcmp(argv[1],"-new") == 0)
	{
		cout << "Running a new simulation" << endl;
		loadParametersForNewSimulation(argv[2]);
	}
	else if(strcmp(argv[1],"-old") == 0)
	{
		cout << "Using an existing binary file" << endl;
		binary = true;
		loadParametersForOldSimulation(argv[2]);
	}
	else
	{
		cout << "Unknown input argument 1" << endl;
		cout << "Running a new simulation" << endl;
		loadParametersForNewSimulation(argv[2]);
	}
	mkdir(saveFolder, S_IRWXU|S_IRGRP|S_IXGRP|S_IROTH);
	chmod(saveFolder, S_IRWXU|S_IRGRP|S_IXGRP|S_IROTH);
	sprintf(simulationStepsFolder, "%s/simulation_steps/", saveFolder);
	mkdir(simulationStepsFolder, S_IRWXU|S_IRGRP|S_IXGRP|S_IROTH);
	chmod(simulationStepsFolder, S_IRWXU|S_IRGRP|S_IXGRP|S_IROTH);

	if(!binary)
	{
		//sprintf(saveFolder, "./T%.3f-g%.3f-mg%.3f-sbt%.3f-%.3f-sbb%.3f-%.3f", skin, tangentialGrowth, middleGrowth, shears[0],bulks[0],shears[1], bulks[1]);
		if(!loadMeshTetgen(meshFile, skin))
		{
			cout << "Mesh file doesn't exist" << endl;
			exit(-1);
		}	
		//mesh optimization
		if(BFS) sortBFS(meshFile, 100);		
		//clean the part in the bulk that is not simulated
		cleanFixedPart();
		//change the fixed properties, useful for one layer simulations
		if(fixedFactor == 1) adjustFixedNodeProperties();
		distanceToGrowthSurfaceAndNormalsTrimesh(growthSurface);
		getConstrainedNodes();
		distanceToFixedSurfaceTrimesh(fixedSurface, skin);
		parameter[0] = stopError; // stop error
		parameter[1] = dampingFactor *  averageSpacing * averageSpacing * averageSpacing; //damping
		parameter[2] = timeStepFactor * averageSpacing; // timestep
		if(bulks[0] > bulks[1])
		{
			parameter[2] /= sqrt(bulks[0]); 
		}
		else
		{
			parameter[2] /= sqrt(bulks[1]); 
		}
		parameter[3] = averageSpacing * averageSpacing * averageSpacing; //average mesh spacing in the initial configuration
		//thickenss of the top layer
		parameter[4] = skin;
		//growth of the top layer
		parameter[5] = tangentialGrowth;
		//growth at the border as a precentage of the total growth
		parameter[6] = 0.9;
		//growth slope factor s
		parameter[7] = 100;
		//average mesh spacing
		parameter[8] = averageSpacing;

		parameter[9] = middleGrowth;
		
		initializeCuda();

	}
	else
	{
		initializeCudaFromBinaryFile(binaryFile);
		parameter[0] = stopError; // stop error
		parameter[1] = dampingFactor *  averageSpacing * averageSpacing * averageSpacing; //damping
		parameter[2] = timeStepFactor * averageSpacing; // timestep
		//here we can also change the stiffness of the skin
		if(bulks[0] > bulks[1])
		{
			parameter[2] /= sqrt(bulks[0]); //time step
		}
		else 
		{
			parameter[2] /= sqrt(bulks[1]); //time step
		}
		
		if(growthAdd > 1 || middleGrowthAdd > 1)
		{
			cout << "Applying more growth "<< growthAdd << " " << middleGrowthAdd << endl;
			addGrowth(growthAdd, middleGrowthAdd);
		}
		setCudaGlobalParameters();
		cout << "Done"<< endl;
		
	}
	pthread_t savingThread;
	cout << "Creating saving thread" << endl;
   	pthread_barrier_init(&youCanCopyBarrier, NULL, 2);
   	pthread_barrier_init(&youCanCopyBarrier, NULL, 2);
   	pthread_barrier_init(&youCanSaveBarrier, NULL, 2);
	if(pthread_create(&savingThread, NULL, savePoints, NULL)) 
	{
		cout << "Error creating thread" << endl;
		return 1;
	}
	copyToHost();
	
   	pthread_barrier_wait(&youCanSaveBarrier);

	int changeParametersCount = 0;
	
	while(true)
	{
		//run saveFrequencyIterations
		for(int i = 0; i < saveFrequency; ++i)
		{
			iteration(it);
			it++;
		}
		pthread_barrier_wait(&youCanCopyBarrier);
		if(stop) break;
		copyToHost();
		savingit = it;
		pthread_barrier_wait(&youCanSaveBarrier);
		changeParametersCount++;
		//manually changing saving frequency and damping to speed up the simulation
		if(changeParametersCount % decreaseFrequency  == 0)
		{
			cout << "-------------------------------------" << endl;
			cout << "Damping changed.." << endl;
			cout << "Old value: " << parameter[1] << endl;
			if(changeParametersCount  <= decreaseCount * decreaseFrequency) changeDamping(dampingMultCount); 
			cout << "New value: " << parameter[1] << endl;
			cout << "-------------------------------------" << endl;
			changeParametersCount++;
		}	
	}
	freeCuda();
	if(pthread_join(savingThread, NULL)) 
	{
		fprintf(stderr, "Error joining thread\n");
		return 2;
	}	
	return 0;
}
